NMD-Tiles is a tile set that can be imported into your favorite hex
editor.  It has been tested with Hex Kit and works reasonably well.
I'm doing this to scratch my own itch to create simple tiles I felt
were missing.  As such, this won't be updated regularly, but might be
updated on request.  File a bug report and try your luck.

These tiles are distributed under the Creative Commons Attribution
Share-Alike license, version 4 or later (CC-BY-SA 4.0).  For details,
see the CC website:

https://creativecommons.org/licenses/by-sa/4.0

The example images in the root directory, such as `four-mountains.png`
are not licensed under the same license, and instead fall under the
Hex Kit tile license.  For details, see CONE's tile license:

https://cone.itch.io/fantasyland

Some tiles in this set form parts of a larger image and are designed
to be used together.  Those tiles are in their own separate
directories and are numbered from bottom to top, left to right, in
row-column format:

                              [20]  [21]

                            [10] [11] [12]

                              [00]  [01]

This allows you to quickly build an image from the bottom up, just by
selecting each tile in turn.

The mountain tiles were created with a brush pen and Pilot Varsity
Extra Fine black disposable fountain pen on 1" hex graph 24lb (90gsm)
paper.  The paper's a little light, but I have 99 more pages to work
through, so I'll make do.

The heraldic designs were painted with water colors on heavy water
color paper.  The lion was based on the Flanders Lion:

https://en.wikipedia.org/wiki/File:Arms_of_Flanders.svg

----

Tips for going from a black and white paper scan to proper hex tiles
with GIMP (gimp.org).  Skip to step 10 to see how to seamlessly get
rid of the ugly white border.

1. Scan your image at 300 DPI or better.

2. Convert it to grayscale: [Image] -> [Mode] -> [Grayscale].

3. Level your image: your scanner isn't perfect, so it'll scan black
   and white along a two separate ranges of colors.  You need to set
   the image's black to above the high black range, and the white to
   beneath the low end of the white range, but only by a little bit.
   Using a black/white threshold isn't recommended, then you'll lose
   all manner of subtlety in the shading.

   [Colors] -> [Levels]

4. Fix any major smudges in your image, they should be pretty apparent
   at this point.  Don't worry about cleaning perfect lines as that'll
   ruin the hand-drawn aspect of the work.

5. Save your image now.

6. Using the free-select tool (F), place points inside the middle of
   each corner of the hex's border.  This should select about half of
   each border.  Go to the [Select] menu and choose [Grow], growing by
   1 pixel each time until it pretty much contains the entire hex.
   This reduces the amount of border cleanup you'll need to do later.

7. Since all your tiles are the same size and shape, save your
   selection as a path, using the Select menu: [Select] -> [To Path].

8. Copy and paste your selected hex as a new image (Ctrl+C,
   Ctrl+Shift+V).

9. Resize the image image's canvas appropriately: [Image] -> [Canvas
   Size], making it about 1.1 times the longest side.  In my case,
   since my image is about 301x345 pixels, I'd choose 380 pixels,
   because 345 * 1.1 is 380.  Make sure to center the image and resize
   all the layers before accepting the resize.

   Now, on to the important bit!

10. Using the Fuzzy Select Tool (U), set the threshold to 50, set
    "Select By" to "Composite", enable the "Select Transparent Areas"
    option before selecting part of the area outside of your hex.

11. Grow your selection by exactly one pixel, just like you did in
    step 6.

12. Using the Select by Color Tool (Shift+O), set the same options you
    used in step 10.  Scroll into the border of your hex and, using
    Subtract mode (hold Ctrl), select the darkest pixel you can find
    to remove all the dark pixels from your selection.

13. Now, cut (Ctrl+X) the remainder of the selection.  This will
    remove any unwanted white border, allowing your tiles to blend
    seamlessly into one another.

14. Repeat steps 10 - 13 for any internal areas of the tile that
    should be transparent.  You can see a good example of how this
    looks in the monsterous-mountains/savoy/01.png file.

15. Save your new hex tile!

16. Go back to your source image and select the Move Tool (M).  Pick
    the "Move Selection" mode, and drag your selection to the next hex
    you want to edit.

These steps probably won't help much if your original is in color.  In
that case, good luck!
